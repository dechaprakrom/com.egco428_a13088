//
//  NewCookiesViewController.swift
//  egco428_ex13088
//
//  Created by Pongdec><MacBook on 10/25/17.
//  Copyright © 2017 Pongdec><MacBook. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import SwiftyJSON

class NewCookiesViewController: UIViewController {

    //
    @IBOutlet weak var wishBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    //
    @IBOutlet weak var closeCookies: UIImageView!
    @IBOutlet weak var openCookies: UIImageView!
    //
    @IBOutlet weak var msg: UILabel!
    //
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    //
    
    var message = ""
    var meaning = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func makewishBtn(_ sender: Any) {
        //Hidden
            closeCookies.isHidden = true
            wishBtn.isHidden = true
            saveBtn.isHidden = false
            openCookies.isHidden = false
        //Alamofire
            Alamofire.request("http://www.atilal.info/cookies.php").responseJSON { response in
                switch response.result {
                case .success:
                    
                    let json = JSON(response.data as Any)
                    if json["meaning"] == "positive"{
                        
                        self.msg.text = "\(json["message"])"
                        self.meaning = "positive"
                        self.message = "\(json["message"])"
                        //ResultMsg
                        self.resultLabel.text = "Result : \(self.message)"
                        //Date
                        let date = Date()
                        self.dateLabel.text = "Date : \(date)"
                        
                    }else{
                        self.msg.text = "\(json["message"])"
                        self.meaning = "negative"
                        self.message = "\(json["message"])"
                        //ResultMsg
                        self.resultLabel.text = "Result : \(self.message)"
                        //Date
                        let date = Date()
                        self.dateLabel.text = "Date : \(date)"
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        
        
    }
   
    @IBAction func unwindBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
        dismiss(animated : true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print("2")
//        print(message)
//        print(meaning)
////            let vc = segue.destination as! ViewController
////            vc.myMessage = message
////            vc.getmean = meaning
//        (segue.destination as! ViewController).myMessage = message
//        (segue.destination as! ViewController).getmean = meaning
        

        
    }
    
    
}
