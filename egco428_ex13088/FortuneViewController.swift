//
//  FortuneViewController.swift
//  egco428_ex13088
//
//  Created by Pongdec><MacBook on 10/25/17.
//  Copyright © 2017 Pongdec><MacBook. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import SwiftyJSON


class FortuneViewController: UIViewController {

    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var resultMsg: UILabel!
//    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var showDate: UILabel!
    var message = ""
    var meaning = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //Alamofire
            Alamofire.request("http://www.atilal.info/cookies.php").responseJSON { response in
                switch response.result {
                case .success:

                        let json = JSON(response.data as Any)
                        if json["meaning"] == "positive"{
                            
                            self.msg.text = "\(json["message"])"
                            self.meaning = "positive"
                            self.message = "\(json["message"])"
                            //ResultMsg
                                self.resultMsg.text = "Result :   \(json["message"])"
                        }else{
                            self.msg.text = "\(json["message"])"
                            self.meaning = "negative"
                            self.message = "\(json["message"])"
                            //ResultMsg
                                self.resultMsg.text = "Result :   \(json["message"])"
                    }
                    

                case .failure(let error):
                    print(error)
                }
            }
        
        
        //Date
            let date = Date()
            showDate.text = "\(date)"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   
    
    
}
